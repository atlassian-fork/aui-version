var semver = require('semver');

function pad (n) {
  n = n.toString();
  while (n.length < 3) {
    n = '0' + n;
  }
  return n;
}

function named (version, testNumber, branch) {
  return {
    releaseVersion: ['0.0.0', version.replace(/\./g, '-'), pad(testNumber), branch].join('-').replace(/\//g, '-'),
    nextVersion: version
  }
}

function test (version, testNumber) {
  return {
    releaseVersion: ['0.0.0', version.replace(/\./g, '-'), pad(testNumber), 'do-not-use'].join('-'),
    nextVersion: version
  }
}

function suffixed (version, suffix, testNumber) {
  testNumber = ~~testNumber || 1;
  var versionNumber = version.slice(0, version.indexOf('-'));
  var releaseVersionNumber = versionNumber + '-' + suffix + '-' + testNumber;
  return {
    releaseVersion: releaseVersionNumber,
    nextVersion: version
  }
}

function alpha (version, testNumber) {
  return suffixed(version, 'alpha', testNumber);
}

function beta (version, testNumber) {
  return suffixed(version, 'beta', testNumber);
}

function inc (version, type) {
  var suffix = version.slice(version.indexOf('-'));
  var versionNumber = version.slice(0, version.indexOf('-'));
  var releaseVersionNumber = versionNumber;
  if (type === 'minor' || type === 'major') {
    releaseVersionNumber = semver.inc(versionNumber, type)
  }
  return {
    releaseVersion: releaseVersionNumber,
    nextVersion: semver.inc(releaseVersionNumber, type) + suffix
  }
}

function patch (version) {
  return inc(version, 'patch')
}

function minor (version) {
  return inc(version, 'minor')
}

function major (version) {
  return inc(version, 'major')
}

module.exports = {
  named: named,
  test: test,
  alpha: alpha,
  beta: beta,
  patch: patch,
  minor: minor,
  major: major
};
